# Voltron IA - Group 16

The City of Namek is about to shatter the way the municipal library is managed. The new expectations of remote access to resources have prompted a virtual library project, so that the audience of the library can access all the offer from a distance. 

The city must also allow monitoring of the archives room conservation conditions, and alert the appropriate department in case of potential risks; the old manuscripts and rare editions it contains could indeed suffer from the lack of in-situ personnel.

## AI perspective

We have imagined 2 solutions to meet the need expressed by the library.

1. **Detect the presence of humans on the library's surveillance cameras**. The goal is to be able to empty a room of its oxygen in case of fire. To that, we used Yolov3.

2. **Provide a book recommendation system**. Like many entertainment platforms, the preferences of other users are analyzed to provide the most relevant recommendations possible. To that, we used TensorFlow. 

See more about in the related playbooks.